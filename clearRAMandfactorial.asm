;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; NAME:    clearRAMandfactorial.asm
; AUTHOR:  
; LASTMOD: 2021.11.23 (lkm)
;
; DESCRIPTION:
;
;     This is the assembly source code that produces machine op code for
;     the factorial of a number. 
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Code segment:

        ORG    06H      ; Set Program Counter to address 0
FACTO:  LDA    VAL4     ; Load accumulator with VAL5 005
        MOV    B,A      ; Count set to 005 (number to factorial)
        MVI    A,1H     ; Accumulator set to 1
MULT:   LXI    H,0H     ; Clear H,L Registers to initialize Partial 
                        ; Product (start of multiply)
        MVI    E,8H     ; New counter, not B register
THERE:  DAD    H        ; Shift Partial Product left into Carry
        RAL             ; Rotate Multiplier Bit to Carry
        JNC    HERE     ; Test Multiplier at Carry
        DAD    B        ; Add Multiplicand to Partial Product if Carry =1
        ACI    0H       ; ADD IMMEDIATE AND CARRY TO ACCUMULATOR
HERE:   DCR    E        ; Decrement Iteration Counter(E)
        JNZ    THERE    ; Check Iterations
        MOV    A,H      ; Put result in the accumulator
        DCR    B        ; Count decrements
        JNZ    MULT     ; If count NOT zero jump
        STA    DONE     ; Store accumulator in address
        RET             ; Return to next part

SUB2:
        ORG    26H
        LXI    B,0H     ; Clear register pair BC
        LXI    D,0H     ; Clear register pair DE
        LXI    H,0H     ; Clear register pair HL
        RET             ; return

SUBROUTINE:
        ORG    30H      ; Load at memory location 51
       ;LXI    D,VAL1   ; Load the register pair DE with the address where we will start replacing the "garbage"
        LDA    VAL1
        MOV    D,A
       ;LXI    B,VAL3   ; The amount of addresses that will be replaced has been loaded to register pair BC
        LDA    VAL3
        MOV    B,A
                        ; VAL3 can't be bigger than 255 because we only use register C
        LDA    VAL4     ; Load the accumulator with the value specified by the first two addresses
        STAX   D        ; Store the accumulator contents in the address specified by register pair DE
        INX    D        ; Increase the register pair DE value by 1
        DCR    C        ; Decreases the C register by one
        RZ              ; Return to the rest of the program if register B is 0
                        ; When register C becomes 0, the zero status bit changes so we can call this instruction 
        JMP     3dH     ; Loop the program
START:
        ORG     42H
        
        CALL    33H     ; Initiate SUBROUTINE
        NOP             ; This will be our MAIN event loop. From here we can call SUBROUTINE as many times as we want
        CALL    28H     ; Initiate SUB2
        NOP
        CALL    06H     ; Initiate FACTO
        NOP
        JMP     4f

; Data segment: 
        ORG    00H

VAL1:   DB     1bH      ; Data Byte at address 0 = 0
                        ; These are the values where we want to start clearing the RAM
VAL2:   DB     0fH      ; Data Byte at address 2 = 15
                        ; VAL3 is how many spaces do we want to clear or set to a NOP operation
VAL3:   DB     00h      ; Data Byte at address 3 = 0
                        ; VAL4 is what we will replace the RAM addresses with
DONE:   DB     0H       ; Data Byte at address 4 = 0
VAL4:   DB     05H      ; Data Byte at address 5 = 5

        END             ; End
