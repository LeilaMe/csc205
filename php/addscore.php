<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
    <title>Guitar Wars - Add Your High Score</title>
    <link rel="stylesheet" type="text/css" href="style.css" /> 
</head>
<body>
    <h2>Guitar Wars - Add Your High Score</h2>
<?php
// Define the upload path and maximum file size constants
    define('GW_UPLOADPATH', 'images/');
    if (isset($_POST['submit'])) {
        // Grab the score data from the POST 
        $name = $_POST['name'];
        $score = $_POST['score'];
        $screenshot = $_FILES['screenshot']['name'];
        if (!empty($name) && !empty($score) && !empty($screenshot)) {
            // Move the file to the target upload folder
            $target = GW_UPLOADPATH . $screenshot;
            if (move_uploaded_file($_FILES['screenshot']['tmp_name'], $target)){
                // Connect to the database
                $dbc = mysqli_connect('www.guitarwars.net', 'admin', 'rockit', 'gwdb');
    // Write the data to the database
    $query = "INSERT INTO guitarwars VALUES (0, NOW(), '$name', '$score')"; 
    mysqli_query($dbc, $query);
    // Confirm success with the user
    echo '<p>Thanks for adding your new high score!</p>';
    echo '<p><strong>Name:</strong> ' . $name . '<br />';
    echo '<strong>Score:</strong> ' . $score . '</p>';
    echo '<p><a href="index.php">&lt;&lt; Back to high scores</a></p>';
    // Clear the score data to clear the form 
    $name = "";
    $score = "";
    mysqli_close($dbc);
    }
    else {
        echo '<p class="error">Please enter all of the information to add ' . 'your high score.</p>'; 
    }
    }
?>
    <hr />
<form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="MAX_FILE_SIZE" value="32768" />
<label for="name">Name:</label>
<input type="text" id="name" name="name" value="<?php if (!empty($name)) echo $name; ?>" />
<br />
<label for="score">Score:</label>
<input type="text" id="score" name="score" value="<?php if (!empty($score)) echo $score; ?>" />
<br />
<label for="screenshot">Screen shot:</label>
<input type="file" id="screenshot" name="screenshot" />
<hr />
<input type="submit" value="Add" name="submit" /> 
</form>
</body>
</html>
