;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; NAME:    Newfactorial.asm
; AUTHOR:  Leila Meng
; LASTMOD: 2021.11.22 (lkm)
;
; DESCRIPTION:
;
;     This is the assembly source code that produces machine op code for
;     the factorial of a number. 
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Code segment:

        ORG    0H       ; Set Program Counter to address 0
START:  MVI    B,5H     ; Count set to 005 (number to factorial)
        MVI    A,1H     ; Accumulator set to 1
MULT:   LXI    H,0H     ; Clear H,L Registers to initialize Partial 
                        ; Product (start of multiply)
        MVI    E,8H     ; New counter, not B register
THERE:  DAD    H        ; Shift Partial Product left into Carry
        RAL             ; Rotate Multiplier Bit to Carry
        JNC    HERE     ; Test Multiplier at Carry
        DAD    B        ; Add Multiplicand to Partial Product if Carry =1
        ACI    0H       ; ADD IMMEDIATE AND CARRY TO ACCUMULATOR
HERE:   DCR    E        ; Decrement Iteration Counter(E)
        JNZ    THERE    ; Check Iterations
        MOV    A,H      ; Put result in the accumulator
        DCR    B        ; Count decrements
        JNZ    MULT     ; If count NOT zero jump
        STA    DONE     ; Store accumulator in address
        RET             ; Return to next part

; Data segment: 

        ORG    80H     ; Set Program Counter to address 200
DONE:   DB     0H       ; Data Byte at address 200 = 0

        END             ; End
