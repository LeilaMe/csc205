; based on goodbye.asm
; Mount emulated disk 2 in drive 0 – switch 12 up, switch 1 up (all other switches down).
; ~/Projects/csc205/disks/DISK02.DSK I think
; https://www.altairduino.com/wp-content/uploads/2017/10/Documentation.pdf
; ~/Projects/csc205/disks/DISKDIR.TXT this one?
; Set SW15-0 to: 0001nnnnDDDDDDDD where nnnn is a 4-bit number selecting the drive (i.e. drive 0-15) and DDDDDDDD is an 8-bit number selecting the disk number and press AUX2 down. 
; so 0001 0000 00000010 for drive 0 and disk 2
; SW15-SW0 Address/Data entry switches 
; ?


; constants

SIO1S:	EQU	10h	; Serial I/O communications port 1 STATUS
SW1     EQU     1002h   ; 0001 0000 00000010 for drive 0 and disk 2
MRST:	EQU	03h	; UART Master Reset 

;Code segment
	ORG	000h	; Load at memory location 000 (hex)

	MVI	A,MRST
	OUT	SIO1S	; Reset the UART
	MVI	A,15h	; Settings: No RI, No XI, RTS Low, 8N1, /16
	OUT	SIO1S	; Configure the UART with above settings
        
        IN      SW1     ;

