# PART 1 INTRODUCTION
- semiconductor technology- silicon is a semiconductor, The ALTAIR 8800 uses a CPU on a single silicon chip, silicon is used in CPUs today
- 78 machine language instructions
- Just remember that a computer does only what its programmer instructs it to do.
# A. LOGIC
- George Boole- ninteenth century British mathematician, any logical statement can be analyzed with simple arithmetic relationships
- Boolean algebra- true or false, perfect for electronic circuits, circuit is the on state means true, in the off state is false. 
- And, or and not. **and: true when all logic conditions are true**, not: reverses the logic statement so true statements are false and false is true. 
- Switch on- true, passes electrical current, numerical value of 1.
- Switch off- false, not pass electrical current, numerical value of 0.
- False AND False - False
- False AND True  - False
- True AND False  - False
- True AND True   - True

AND Function Truth Table
| A | B | OUT |
|---|---|-----|
| 0 | 0 | 0   |
| 0 | 1 | 0   |
| 1 | 0 | 0   |
| 1 | 1 | 1   |

OR Function Truth Table
| A | B | OUT |
|---|---|-----|
| 0 | 0 | 0   |
| 0 | 1 | 1   |
| 1 | 0 | 1   |
| 1 | 1 | 1   |

NOT Function Truth Table
| A | OUT |
|---|-----|
| 0 | 1   |
| 1 | 0   |
# B. ELECTRONIC LOGIC
- AND logic symbol looks like the letter D.
- OR logic symbol looks like a round chevron?
- NOT logic symbol looks like a triangle
- they go left to right, not only has a like in the chart, the NOT symbol has an open circle on the right side. 
NAND (NOT-AND) and NOR (NOT-OR)

NAND Function Truth Table
| A | B | OUT |
|---|---|-----|
| 0 | 0 | 1   |
| 0 | 1 | 1   |
| 1 | 0 | 1   |
| 1 | 1 | 0   |

NOR Function Truth Table
| A | B | OUT |
|---|---|-----|
| 0 | 0 | 1   |
| 0 | 1 | 0   |
| 1 | 0 | 0   |
| 1 | 1 | 0   |

- Three or more logic circuits make a logic system
- EXCLUSIVE-OR circuit binary adder, used to implement logical functions and used to add two input conditions, electronic logic circuits are compatible with the binary number system
- 1 and 1 means 1, not 1 means 0, 1 or 1 means 1, 0 and 1 means 1 why is it 0. it is 10 but why did I get 11. 
- two NAND circuits can be connected to form a bistable circuit called a flip-flop
- changes state only when an incoming signal in the form of a pulse arrives, it acts as a short term memory element
- Several flip-flops can be cascaded together to form electronic counters and memory registers.
- logic circuits can be connected together
- Monostable circuits occupy one of two states unless an incoming pulse is received They then occupy an opposite state for a brief time and then resume their normal state
- Astable circuits continually switch back and forth between two states
# C. NUMBER SYSTEMS
- Number systems can be based on any number
- a digit in a two digit system is a bit
- bit is binary digit
- complex logic systems like computers can uses number systems with base eight and sixteen because they can express binary numbers shortly
# D. THE BINARY SYSTEM
-  ALTAIR 8800 performs nearly all operations in binary
- A typical binary number processed by the computer incorporates 8-bits
- A fixed length binary number such as this is usually called a word or byte
- byte 8 bits
- computers are usually designed to process and store a fixed number of words (or bytes)
- Record successive digits for each count in a column. When the total number of available digits has been used, begin a new column to the left of the first and resume counting.
- Each bit in a binary number indicates by which power of two the number is to be raised

